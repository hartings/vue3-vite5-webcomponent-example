import './assets/main.css'

import { createApp } from 'vue'
import { createPinia } from 'pinia'
import App from './App.vue'
//import App from './eCharts-wo-wc.vue'

const app = createApp(App)

app.use(createPinia())

app.mount('#app')
